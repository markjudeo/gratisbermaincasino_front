<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class userModel extends Model
{
    //
    protected $connection = 'mysql';
    protected $primaryKey = 'id';
    protected $table = 'user';
    protected $fillable = array(
    	'user_name', 
    	'password', 
    	'email'
    	);
    public $timestamps = true;
}
