<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\gameModel as game;
use Jenssegers\Agent\Agent;

class LobbyController extends Controller
{
    //

    function index(){

    	$data['game'] = game::whereIn('provider',['TTG','VIVO'])->where(array('is_active'=>'1' , 'can_mobile' => '1' , 'category' => 'SLOT'))->orderBy('level' , 'desc')->limit(21)->get();
    	return view('pages.lobby')->with('data', $data);
    }

    function free_play($name){
        $agent = new Agent();

        $name = str_replace('_', ' ', $name);
        $val = game::where('name' , $name)->first();
        $data['device'] = $agent->platform();

        $provider = $val->provider;

       switch ($provider) {
           case 'VIVO':
           $data['free_play_url']='http://gspotslots.bbtech.asia/onlinecasino/GetGames/GetGameDemo?config=vivo_en&gameconfig='.$val->mobile_config.'&lobby=http://gratisbermaincasino.com';
               break;
           
           default:
              $data['free_play_url']='http://pff.ttms.co/casino/default/game/casino5.html?playerHandle=999999&account=FunAcct&gameName='.$val->mobile_config.'&gameType=0&gameId='.$val->mobile_id.'&lang=en&lsdId=asialive88.com&deviceType=mobile&t=1493697096884';
               break;
       }
        
        $data['target_casino'] = $val->target_casino;
        
        return view('pages.launch_game')->with('data',$data);
    }
    // function index(){

    //     $data['TTG'] = game::where(array('is_active'=>'1' , 'provider' => 'TTG' , 'can_mobile' => '1' , 'category' => 'SLOT'))->orderBy('level' , 'desc')->limit(27)->get();
    //     $data['VIVO'] = game::where(array('is_active'=>'1' , 'provider' => 'VIVO' , 'can_mobile' => '1' , 'category' => 'SLOT'))->orderBy('level' , 'desc')->limit(27)->get();
    //     return view('pages.lobby')->with('data', $data);
    // }
    
}
