$(document).ready(function(){
  $(".owl-carousel").owlCarousel({
    singleItem: true,
    autoplay:true,
    autoplayTimeout:4000,
    items:1
  });



  //set url to iframe
  // $(".launch_game").click(function(){

  //   var input = $( this );
  //   var url_game = input.attr( "game_launch" );
  //   console.log(url_game);

  //   set_url(url_game)

  //   $('#myFrame').val(input.attr( "game_launch" ));
  //   $(".hide_all").hide();
  //   $(".show_frame").show();
  //   setTimeout(function(){
  //    $.colorbox({
  //     inline:true,
  //     width: "90%", 
  //     href:"#inline_content",
  //     fixed:true}); 
  //   }, 15000);
  // }); 
    
});
// $("#back_button").click(function(){
//   $(".show_frame").hide();
//   $(".hide_all").show();    
// })



document.getElementById('pre_form_register').onsubmit= function(e) {
  e.preventDefault();

  var data = $("#pre_form_register").serialize();

  $(document).ajaxStart(function(){
    $(".loading-mask").css("display", "block");
    $("#submit_fields").prop('disabled', true);
  });
  $.ajax({
   data: data,
   dataType: 'json',
   type: "POST",
   url: "/addUsers",
   beforeSend: function(xhr) {xhr.setRequestHeader('X-CSRF-Token', $('input[name="_token"]').val())},
   success: function(a){
     sendParam(a.url + a.token);

   }

 });
};



function set_url(url){
  document.getElementById('myFrame').src = url
}
$(".inline").colorbox({inline:true, width:"90%"});
function sendParam(a){
  location.href=a;
}


function validate_user_id(){
  var str=$("#username").val();
  str=str.replace(/[^a-zA-Z 0-9]+/g,'');
  str=str.replace(/ /g,"");
  str=str.toUpperCase();
  $("#username").val(str);
  if(str!=""){
    if(str.length >= 4){
      if (str.match(/[a-z A-Z]/i)) {
        $.get("https://asialive88.com/user/validate_user_id/"+str, function(r){
          if(r=="1"){
            $("#username").css({"background":"#800000","color":"yellow","border":"1px solid #ff0000"}).attr({"title":"Username tersebut sudah digunakan! Silahkan gunakan username lain."});
            $("#name_invalid").show();
            $("#name_invalid2").hide();
            $("#name_invalid3").hide();
            $('#submit_fields').prop('disabled', true);
          }
          else if(str.match(/[a-z A-Z]/i)) {
            $.get("/validate_user_id/user_name/"+str, function(r){
              if(r=="1"){
                $("#username").css({"background":"#800000","color":"yellow","border":"1px solid #ff0000"}).attr({"title":"Username tersebut sudah digunakan! Silahkan gunakan username lain."});
                $("#name_invalid").show();
                $("#name_invalid2").hide();
                $("#name_invalid3").hide();
                $('#submit_fields').prop('disabled', true);
              }else{
                $("#username").css({"background":"#FFFFFF","color":"#000000","border":"1px solid #ccc"}).attr({"title":""});
                $("#name_invalid").hide();
                $("#name_invalid2").hide();
                $("#name_invalid3").hide();
                if(
                  !$("#fname_invalid").is(":visible") && 
                  !$("#fname_invalid").is(":visible") && 
                  !$("#email_invalid").is(":visible") && 
                  !$("#phone_invalid").is(":visible") &&  
                  !$("#name_invalid").is(":visible") &&
                  !$("#name_invalid2").is(":visible") &&
                  !$("#name_invalid3").is(":visible") && 
                  !$("#password2_invalid").is(":visible") && 
                  !$("#password_invalid").is(":visible")){
                  $('#submit_fields').prop('disabled', false); 
              }
            }
          })

          }else{
            $("#username").css({"background":"#FFFFFF","color":"#000000","border":"1px solid #ccc"}).attr({"title":""});
            $("#name_invalid").hide();
            $("#name_invalid2").hide();
            $("#name_invalid3").hide();
            if(
              !$("#fname_invalid").is(":visible") && 
              !$("#fname_invalid").is(":visible") && 
              !$("#email_invalid").is(":visible") && 
              !$("#phone_invalid").is(":visible") &&  
              !$("#name_invalid").is(":visible") &&
              !$("#name_invalid2").is(":visible") &&
              !$("#name_invalid3").is(":visible") && 
              !$("#password2_invalid").is(":visible") && 
              !$("#password_invalid").is(":visible")){
              $('#submit_fields').prop('disabled', false); 
          }
          }
        })
      }
      else{
        $("#username").css({"background":"#800000","color":"yellow","border":"1px solid #ff0000"}).attr({"title":"Username harus mengandung huruf!"});
        $("#name_invalid3").hide();
        $("#name_invalid2").show();
        $("#name_invalid").hide();
        $('#submit_fields').prop('disabled', true);
      }
    }
    else{
      $("#username").css({"background":"#800000","color":"yellow","border":"1px solid #ff0000"}).attr({"title":"Username minimal 4 karakter alfanumerik!"});
      $("#name_invalid3").show();
      $("#name_invalid2").hide();
      $("#name_invalid").hide();
      $('#submit_fields').prop('disabled', true);
    } 
  }else{
      $("#username").css({"background":"#FFFFFF","color":"#000000","border":"1px solid #ccc"}).attr({"title":""});
      $("#name_invalid").hide();
      $("#name_invalid2").hide();
      $("#name_invalid3").hide();
       if(
     !$("#fname_invalid").is(":visible") && 
     !$("#fname_invalid").is(":visible") && 
     !$("#email_invalid").is(":visible") && 
     !$("#phone_invalid").is(":visible") &&  
     !$("#name_invalid").is(":visible") &&
     !$("#name_invalid2").is(":visible") &&
     !$("#name_invalid3").is(":visible") && 
     !$("#password2_invalid").is(":visible") && 
     !$("#password_invalid").is(":visible")){
            $('#submit_fields').prop('disabled', false); 
     }
    } 
}
function validate_password(){
  var pass = $("#password").val();
  if(pass){
    if(pass.length >= 6){
      $("#password").css({"background":"#FFFFFF","color":"#000000","border":"1px solid #ccc"}).attr({"title":""});
      $("#password_invalid").hide();
       if(
     !$("#fname_invalid").is(":visible") && 
     !$("#fname_invalid").is(":visible") && 
     !$("#email_invalid").is(":visible") && 
     !$("#phone_invalid").is(":visible") &&  
     !$("#name_invalid").is(":visible") &&
     !$("#name_invalid2").is(":visible") &&
     !$("#name_invalid3").is(":visible") && 
     !$("#password2_invalid").is(":visible") && 
     !$("#password_invalid").is(":visible")){
            $('#submit_fields').prop('disabled', false); 
     }
    }else{
      $("#password").css({"background":"#800000","color":"yellow","border":"1px solid #ff0000"}).attr({"title":"Password minimal 6 karakter alfanumerik!"});
      $("#password_invalid").show();
      $('#submit_fields').prop('disabled', true);
    } 
  }else{
      $("#password").css({"background":"#FFFFFF","color":"#000000","border":"1px solid #ccc"}).attr({"title":""});
      $("#password_invalid").hide();
       if(
     !$("#fname_invalid").is(":visible") && 
     !$("#fname_invalid").is(":visible") && 
     !$("#email_invalid").is(":visible") && 
     !$("#phone_invalid").is(":visible") &&  
     !$("#name_invalid").is(":visible") &&
     !$("#name_invalid2").is(":visible") &&
     !$("#name_invalid3").is(":visible") && 
     !$("#password2_invalid").is(":visible") && 
     !$("#password_invalid").is(":visible")){
            $('#submit_fields').prop('disabled', false); 
     }
   } 
 }

 function popupLogin(){
  $('#provider2').val($('#gameFrame').attr('casino'));
  setTimeout(function(){
    SecondPopup();
    $.colorbox({
      inline:true,
      width: "90%", 
      href:"#inline_content"}); 
  }, 45000);
    setTimeout(function(){
      if (document.cancelFullScreen) {  
        document.cancelFullScreen();  
      } else if (document.mozCancelFullScreen) {  
        document.mozCancelFullScreen();  
      } else if (document.webkitCancelFullScreen) {  
        document.webkitCancelFullScreen();  
      } 
    },44000);
  }
  
function SecondPopup(){

  setTimeout(function(){
    LastPopup();
    $.colorbox({
      inline:true,
      width: "90%", 
      href:"#inline_content"}); 
  }, 60000);
  setTimeout(function(){
      if (document.cancelFullScreen) {  
        document.cancelFullScreen();  
      } else if (document.mozCancelFullScreen) {  
        document.mozCancelFullScreen();  
      } else if (document.webkitCancelFullScreen) {  
        document.webkitCancelFullScreen();  
      } 
    },69900);
}
function LastPopup(){

  setTimeout(function(){
    LastPopup();
    $.colorbox({
      inline:true,
      width: "90%", 
      href:"#inline_content"}); 
  }, 120000);
  setTimeout(function(){
      if (document.cancelFullScreen) {  
        document.cancelFullScreen();  
      } else if (document.mozCancelFullScreen) {  
        document.mozCancelFullScreen();  
      } else if (document.webkitCancelFullScreen) {  
        document.webkitCancelFullScreen();  
      } 
    },119900);
}


  // function toggleFullScreen() {
  //   var i = document.getElementById("gameFrame");

  //   if (
  //     document.fullscreenEnabled || 
  //     document.webkitFullscreenEnabled || 
  //     document.mozFullScreenEnabled ||
  //     document.msFullscreenEnabled
  //     ) 
  //   { 
  //     if (i.requestFullscreen) {
  //       i.requestFullscreen();
  //     } else if (i.webkitRequestFullscreen) {
  //       i.webkitRequestFullscreen();
  //     } else if (i.mozRequestFullScreen) {
  //       i.mozRequestFullScreen();
  //     } else if (i.msRequestFullscreen) {
  //       i.msRequestFullscreen();
  //     }else if (i.msRequestFullscreen){
  //       i.webkitEnterFullscreen();
  //     }
  //   }
  //   else {  
  //     if (document.exitFullscreen) {
  //       document.exitFullscreen();
  //     } else if (document.webkitExitFullscreen) {
  //       document.webkitExitFullscreen();
  //     } else if (document.mozCancelFullScreen) {
  //       document.mozCancelFullScreen();
  //     } else if (document.msExitFullscreen) {
  //       document.msExitFullscreen();
  //     }  
  //   }  
  // }


  // var toggle = document.getElementById('toggle');
  // addEventListener("click", function() {
  //   var el = document.getElementById('iframe_container'),
  //   rfs = el.requestFullscreen
  //   || el.webkitRequestFullScreen
  //   || el.mozRequestFullScreen
  //   || el.msRequestFullscreen 
  //   ;

  //   rfs.call(el);
  // });

var element = document.getElementById('iframe_container');
function isFullScreen()
{
    return (document.fullScreenElement && document.fullScreenElement !== null)
         || document.mozFullScreen
         || document.webkitIsFullScreen;
}


function requestFullScreen(element)
{
    var element = document.getElementById('iframe_container');
    if (element.requestFullscreen)
        element.requestFullscreen();
    else if (element.msRequestFullscreen)
        element.msRequestFullscreen();
    else if (element.mozRequestFullScreen)
        element.mozRequestFullScreen();
    else if (element.webkitRequestFullscreen)
        element.webkitRequestFullscreen();
}

function exitFullScreen()
{
    if (document.exitFullscreen)
        document.exitFullscreen();
    else if (document.msExitFullscreen)
        document.msExitFullscreen();
    else if (document.mozCancelFullScreen)
        document.mozCancelFullScreen();
    else if (document.webkitExitFullscreen)
        document.webkitExitFullscreen();
}

function toggleFullScreen(element)
{
    if (isFullScreen())
        exitFullScreen();
    else
        requestFullScreen(element || document.documentElement);
}


  
