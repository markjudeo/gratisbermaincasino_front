<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGameTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('game_table', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('desktop_id');
            $table->string('mobile_id');
            $table->string('desktop_config');
            $table->string('mobile_config');
            $table->tinyInteger('type');
            $table->tinyInteger('can_desktop');
            $table->tinyInteger('can_mobile');
            $table->string('image');
            $table->string('level');
            $table->string('provider');
            $table->string('category');
            $table->tinyInteger('is_active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('game_table');
    }
}
