<!DOCTYPE html>
<html>
<head>
	<title></title>

	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

	<meta property="og:description" content="Gratis bermain casino online indonesia, banyak pilihan Game Casino dan mainkan Game Kesukaan Kamu di sini"/>
	<meta property="og:url" content="http://gratisbermaincasino.com"/>
	<meta property="og:site_name" content="gratisbermaincasino"/>
	<meta property="og:image" content="http://gratisbermaincasino.com/images/header/logo.png"/>

	<meta name="twitter:card" content="summary"/>
	<meta name="twitter:title" content="Casino | Casino Online | Casino Indonesia | Casino Games by gratisbermaincasino.com"/>
	<meta name="twitter:description" content="Gratis bermain casino online indonesia, banyak pilihan Game Casino dan mainkan Game Kesukaan Kamu di sini"/>
	<meta name="twitter:site" content="@gratisbermaincasino"/>
	<meta name="twitter:image" content="http://gratisbermaincasino.com/images/header/logo.png"/>

	<meta name="author" content="gratisbermaincasino.com">
	<meta name="robots" content="index, follow" />
	<meta name="copyright" content="gratisbermaincasino.com" />
	<meta name="geo.placename" content="Jakarta" />
	<meta name="geo.region" content="ID-JK" />
	<meta name="geo.country" content="ID" />
	<meta name="language" content="ID" />
	<meta name="tgn.nation" content="Indonesia" />
	<meta name="rating" content="general" />
	<meta name="distribution" content="global" />
	<meta name="author" content="gratisbermaincasino.com" />
	<meta name="publisher" content="gratisbermaincasino.com" />
	<meta name="copyright" content="copyright@ 2017 gratisbermaincasino.com" />
	<meta name="DC.Publisher" content="gratisbermaincasino.com" >
	<link rel="canonical" href="http://gratisbermaincasino.com/" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
	<link rel="icon" href="{{ asset('images/header') }}/favicon.png" type="image/x-icon">
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/owl.carousel.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/colorbox.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/custom.css') }}">
</head>
<body>
	<div class="loading-mask">
		<div class="loading-image">
			<img src="{{ asset('images/loader') }}/loading.gif">
			<span>Loading...</span>
		</div>
	</div>
	@yield('content')
	
	<div class="login_popup">
		<div id='inline_content' class="bg-padding">
			<div class="width">
				<div class="row">
					<div class="title_register">
						<h2 class="title_register_text">Register for Real Play</h2>
					</div>
				</div>
				<form method="POST" id="pre_form_register" >
					{{ csrf_field() }}
					<div class="form-group row">
						{{-- <label for="username" class="col-2 col-form-label label-size">Username</label> --}}
						<div class="col-10">
							<input class="form-control input" type="text" id="username" onblur="validate_user_id()" maxlength="15" autocomplete="off" placeholder="Username" name="username" required>
						</div>
					</div>
					<div>
						<div class="validation_message1" id="name_invalid">Username tersebut sudah digunakan! Silahkan gunakan username lain!</div>
						<div class="validation_message2" id="name_invalid2">Username harus mengandung huruf!</div>
						<div class="note validation_message3"  id="name_invalid3">Username minimal 4 karakter alfanumerik!</div>
					</div>
					<div class="form-group row">
						{{-- <label for="password" class="col-2 col-form-label label-size">Password</label> --}}
						<div class="col-10">
							<input class="form-control input" autocomplete="off" placeholder="Password" type="password" onblur="validate_password()" id="password" name="password" required>
						</div>
					</div>
					<div>
						<div class="note validation_message4" id="password_invalid">Password minimal 6 karakter alfanumerik!</div>
					</div>
					<div class="form-group row">
						{{-- <label for="email" class="col-2 col-form-label label-size">Email</label> --}}
						<div class="col-10">
							<input class="form-control input" autocomplete="off" placeholder="Email" type="email" id="email" name="email" required>
						</div>
					</div>
					<input type="hidden" name="casino" value="" id="provider2">
					<div class="form-group row">
						<button class="btn btn-daftar full-width" type="submit" id="submit_fields">SUBMIT</button>
					</div>
				</div>
			</form>
			</div>
		</div>
	</div>
	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

		ga('create', 'UA-98440119-1', 'auto');
		ga('send', 'pageview');

	</script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="{{ asset('js/themes') }}/owl.carousel.min.js"></script>
	
	<script type="text/javascript" src="{{ asset('js/themes') }}/jquery.colorbox-min.js"></script>
	<script type="text/javascript" src="{{ asset('js/jquery.jcryption.3.1.0.js') }}"></script>
	<script type="text/javascript" src="{{ asset('js/custom.js') }}"></script>
</body>
</html>