@extends('../layout.front')
@section('content')


<div class="container">
	<div class="row add-margin bg-game">
		@foreach($data['game'] as $val)


		<div class="game-box col-xs-4 decrease-padding">
		<?php $name = str_replace(' ', '_', $val->name); ?>
			<a href="{{ URL::to('/play') }}/<?= $name;?>" target="_blank" provider="{{ $val->provider }}" class="launch_game">
				<img class="game-image" alt="{{ $val->name }} by GratisBermainCasino" src="{{ asset('images/games') }}/{{ $val->image }}">
				<div class="button-container">
					<a href="javascript:void(0);" class="btn btn-sm btn-free-play">Free Play</a>
				</div>
			</a>
		</div>
		@endforeach	
	</div>		
</div>
@endsection