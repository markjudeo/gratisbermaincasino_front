@extends('../layout.launch')
@section('content')

<div class="rotate-mask">
	<div class="center">
		<img class="rotate-gif" src="{{ asset('images/launch-game') }}/rotate.gif">
		<h3 class="instruction">Turn your device</h3>
	</div>
	
</div>
<div class="iframe-container" id="iframe_container" >
	
	@if ($data['device'] == 'iOS' )
		<div class="absolute">
		<a href="javascript:void(0);" class="addPadding" onclick="toggleFullScreen()">
			<i class="fa fa-angle-right fa-full"></i>
		</a>
	</div>
	@else
	<div class="absolute">
		<a href="javascript:void(0);" class="addPadding" onclick="toggleFullScreen()">
			<i class="fa fa-arrows-alt fa-full"></i>
		</a>
	</div>
	@endif
	
	<iframe src="{{ $data['free_play_url'] }}" class="myFrame" onload="popupLogin()" frameborder="0" casino = "{{ $data['target_casino'] }}" id="gameFrame">
	</iframe>
</div>

@endsection