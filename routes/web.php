<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('pages.lobby');
// });


Route::get('/', 'LobbyController@index');
Route::get('/play/{id}', 'LobbyController@free_play');
Route::post('/addUsers', 'UserController@submitUser');
Route::get('/validate_user_id/{field}/{value}', 'UserController@validateFields');
Route::get('/getFields/{token}', 'UserController@getFields');
Route::post('/updateRegistration', 'UserController@updateRegistration');
